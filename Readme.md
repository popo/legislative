Obtenir la liste des candidats pirates sur toute la France

https://www.partipirate.org/la-liste-des-pirates-aux-legislatives-2017

```bash
wget https://www.partipirate.org/la-liste-des-pirates-aux-legislatives-2017
```

```bash
cat la-liste-des-pirates-aux-legislatives-2017 |egrep -o  "<li>.* - candidat"|sed 's/<li>//g'|sed 's/ - candidat//g' > listeCandidats.txt
```


Résultats par circonscription
```bash
http://elections.interieur.gouv.fr/legislatives-2017/072/07203.html
```

Résultats par commune
```bash
http://elections.interieur.gouv.fr/legislatives-2017/072/07203299.html
```

Correspondance commune<->circo
```bash
wget https://www.data.gouv.fr/s/resources/circonscriptions-legislatives-table-de-correspondance-des-communes-et-des-cantons-pour-les-elections-legislatives-de-2012-et-sa-mise-a-jour-pour-les-elections-legislatives-2017/20170411-141128/Table_de_correspondance_circo_legislatives2017-1.xlsx
```

# H2 Extraction des numéros INSEE des communes de la troisième circonscription
```bash
egrep "^72,"  Table_de_correspondance_circo_legislatives2017-1.csv |awk -F "," '{ printf "%s,%03d\n", $5, $3 }'| grep ^3 > listeComCirco03.txt
```

# H3 Récupération des résultats par commune
```bash
for i in `awk -F "," '{print $2}' listeComCirco03.txt ` ; do wget  http://elections.interieur.gouv.fr/legislatives-2017/072/07203$i.html -o com$i.html; done

cat *.html|grep VALLAS -A4|sed 's/<td style="text-align://g'

```

numéro de commune sur 3 chiffre
```bash
echo 1 | awk '{ printf("%03d\n", $1) }'

```